Introduction
============
This project is used to demonstrate the usage of:

* unit tests
* integration tests
* stubs
* mocks
* design principles and patterns for testable code

The talk belonging to this demo is described in the [Powerpoint](Integrationtesting-Mocks-Stubs.pptx)

This repository has several branches:
* the current one, _master_ containing only the Powerpoint and Astah files
* empty-project, a branch with the skeleton code for the application
* notification-service, a sub-branch from empty-project containing the notification-service component
* log-service, a sub-branch from empty-project containing the log-service component
* log-analyzer, a sub-branch from empty-project containing the log-analyzer component
* partial-results, a branch containing the full (not partial) results: the source code, unit and integration tests but also a Workshop.html that can be used to reproduce the contents of this branch using the empty-project. 
